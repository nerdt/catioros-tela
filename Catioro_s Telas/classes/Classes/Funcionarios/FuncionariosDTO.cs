﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catiotro_s.classes.Classes.Cliente
{
   public class FuncionarioDTO
    {
        public int Id { get; set; }
        public int IdFuncionario { get; set; }
        public string Cargo { get; set; }
        public int Salario { get; set; }
        public string Especialidade { get; set; }
        public int IdLog { get; set; }
        public int IdFolhaPag { get; set; }
    }
}
