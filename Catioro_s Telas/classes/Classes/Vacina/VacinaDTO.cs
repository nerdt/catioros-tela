﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catiotro_s.classes.Classes.Agenda
{
   public class VacinaDTO
    {
        public int Id { get; set; }
        public string Tipo { get; set; }
        public string Prioridade { get; set; }
        public string QtdDose { get; set; }
    }
}
