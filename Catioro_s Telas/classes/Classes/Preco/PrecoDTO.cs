﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catiotro_s.classes.Classes.Agenda
{
   public class PrecoDTO
    {
        public int Id { get; set; }
        public int IdProduto { get; set; }
        public string ProdutoCompra { get; set; }
    }
}
