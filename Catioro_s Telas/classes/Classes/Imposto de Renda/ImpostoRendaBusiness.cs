﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catiotro_s.classes.Classes.Agenda
{
   public class ImpostoRendaBusiness
    {
        public int Salvar(ImpostoRendaDTO impostorenda)
        {
            ImpostoRendaDatabase DB = new ImpostoRendaDatabase();
            int id = DB.Salvar(impostorenda);
            return id;
        }
        public void Alterar(ImpostoRendaDTO impostorenda)
        {
            ImpostoRendaDatabase DB = new ImpostoRendaDatabase();
            DB.Alterar(impostorenda);
        }
        public void Remover(int idimpostorenda)
        {
            ImpostoRendaDatabase DB = new ImpostoRendaDatabase();
            DB.Remover(idimpostorenda);
        }
        public List<ImpostoRendaDTO> Listar()
        {
            ImpostoRendaDatabase DB = new ImpostoRendaDatabase();
            List<ImpostoRendaDTO> impostorenda = DB.Listar();
            return impostorenda;
        }
    }
}
